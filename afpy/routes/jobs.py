from flask import Blueprint
from flask import redirect


jobs_bp = Blueprint("jobs", __name__)


@jobs_bp.route("/emplois/<int:post_id>")
def jobs_render(post_id: int):
    return redirect("https://discuss.afpy.org/c/emplois/14")


@jobs_bp.route("/emplois")
@jobs_bp.route("/emplois/page/<int:current_page>")
def jobs_page(current_page: int = 1):
    return redirect("https://discuss.afpy.org/c/emplois/14")


@jobs_bp.route("/emplois/new", methods=["GET"])
def new_job():
    return redirect("https://discuss.afpy.org/c/emplois/14")
