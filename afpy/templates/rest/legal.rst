================
Mentions légales
================


L’association
=============

L’association « A.F.P.Y. » , Association Francophone Python, fondée le 11
décembre 2004 sous le régime de la loi du 1er juillet 1901 a pour but la
vulgarisation auprès d’un public francophone du langage de programmation python
et de ses applications.


Les documents administratifs
============================

L'ensemble des documents administratifs publics sont disponibles sur `notre
dépôt Git <https://git.afpy.org/AFPy/gestion>`_, en particulier :

- `les statuts
  <https://git.afpy.org/AFPy/gestion/src/branch/master/statuts/2023/statuts.rst>`_,
- `le règlement intérieur
  <https://git.afpy.org/AFPy/gestion/src/branch/master/statuts/2023/reglement.rst>`_,
- `le code de conduite
  <https://git.afpy.org/AFPy/gestion/src/branch/master/statuts/2023/conduite.rst>`_,
- la comptabilité,
- la facturation,
- les comptes rendus d'assemblées générales ; et
- les comptes rendus du comité directeur.
