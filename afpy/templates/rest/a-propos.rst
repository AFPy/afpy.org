=================
Qui sommes-nous ?
=================


L’association
=============

L’association « A.F.P.Y. » , Association Francophone Python, fondée le 11
décembre 2004 sous le régime de la loi du 1er juillet 1901 a pour but la
vulgarisation auprès d’un public francophone du langage de programmation python
et de ses applications.


Bureau et Comité Directeur
==========================

Les membres du comité directeur 2024 sont :

- Présidente : Lucie Anglade (presidence@afpy.org)
- Marc Debureaux (Vice-président)
- Antoine Rozo (secretaire@afpy.org)
- Laurine Leulliette (Vice-secrétaire)
- Pierre Bousquié (tresorerie@afpy.org)
- Thomas Bouchet (Vice-trésorier)
- Agnès Haaser
- Julie Rymer
- Paul Guichon
