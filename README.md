# AFPy.org

Site Web de l'AFPy.


## Lancer localement

Commencez par un `make install`.

Ensuite, `mv .env.template .env` en remplacant les valeurs
nécéssaires.

Créez le répertoire des images: `images` à la racine du projet (ou
ailleurs, configuré via `IMAGES_PATH` dans le `.env`).

Puis un `make serve` suffit pour jouer localement.

⚠ Le login admin par défaut est `admin:password`.


## Tester

```bash
make test
```

## Publier

```bash
make publish
```
